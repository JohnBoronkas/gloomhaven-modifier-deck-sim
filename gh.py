import collections
import random

deck = []
deck += ["+1"] * 5 # 5
deck += ["+2"] * 1 # 1
deck += ["*2"] * 1 # 1
deck += ["0"] * 6 # 6
deck += ["-1"] * 1 # 5
deck += ["-2"] * 1 # 1
deck += ["miss"] * 1 # 1

deck += ["+1&"] * 0 # 0

hands = []
numDraws = 10
for runIdx in range(50_000):
    random.shuffle(deck)
    cardIdx = 0
    drawIdx = 0
    hand = []
    while drawIdx < numDraws:
        card = deck[cardIdx]
        hand.append(card)
        cardIdx += 1

        if (card == "*2" or card == "miss"):
            random.shuffle(deck)
            cardIdx = 0
        elif (card == "+1&"):
            continue

        hands.append(' '.join(hand))
        drawIdx += 1
        hand = []

counter = collections.Counter(hands)
total = sum(counter.values())

for k, v in reversed(sorted(counter.items(), key=lambda item: item[1])):
    print(k + ": " + str(round((v/total) * 100, 2)) + "%")
